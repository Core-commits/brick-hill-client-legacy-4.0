{
    "id": "fa49cb4c-be75-418c-a907-a3d2fadc47d0",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_bold",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 1,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4d15f225-d105-4ac6-a406-43b69e9cafc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 176,
                "y": 62
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "77abaf55-f747-4df5-ba66-b544ba3219e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 215,
                "y": 62
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "369c6885-6806-46e5-80ed-19e16b3c41db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 52,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "7ab68cc9-7fcf-4b59-ae4e-38236de43aed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 134,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "4a28e4b7-f0bb-417b-afff-0efeaa5d8aa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 42
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ba348cde-c83d-499d-aa2a-f74fb8c6a379",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b9f50bad-f269-4682-80be-55a8b635194c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "bbcf41c9-8f25-4224-935b-9ad01194e56f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 194,
                "y": 62
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "32d737c1-04a9-4b59-9648-aa0374d83c01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 141,
                "y": 62
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "9657f39e-eb20-45b5-8c69-480990982ea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 169,
                "y": 62
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "5818de47-3069-49bc-9870-9c08e4cf5a11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 69,
                "y": 62
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "30a183be-3891-46f9-b116-0832d5011247",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 42
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "ba68e175-59b7-4ad3-9663-d7ceb50502b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 182,
                "y": 62
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "cb31b655-8461-4987-a511-114619dc1a15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 77,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ee471dc5-ade8-4639-8137-90c80757c2f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 210,
                "y": 62
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "20f2c9a5-7a42-47ce-939c-d3d39a1ced8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 85,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "000a468b-08f1-48d0-9f9b-e04f44d0542c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 42
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "cdefd0fc-ce1d-457b-a91a-4ac3390c893c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 93,
                "y": 62
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e01d8867-2994-4845-86a9-0a9d4cba847a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 42
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "eb3d64ff-4795-4d02-bc21-efd92559bd77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 42
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3b509735-72ea-4a90-abe5-8ca63c9d4502",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 42
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "44d5ddef-1259-4e8f-a65d-479c3ec11062",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 123,
                "y": 42
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "422ad58c-cf73-4d97-aeb7-7d891347b1f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 167,
                "y": 42
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "bd82e6ee-cd7e-41f1-bd58-86c8e24cf2eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 145,
                "y": 42
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8fe64c8c-ea29-4c08-b0bb-4353f3b69ed1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 156,
                "y": 42
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ad37faa1-a251-4e07-b5d1-1e9d85cb457b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 233,
                "y": 42
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "2f5ba533-40d8-429c-878a-1ff9a33c5afa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 220,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "69b5c276-6ebc-450d-a883-f563d0222c52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 225,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "c79cafb3-e235-4388-a3a4-ace82fb0f0b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 178,
                "y": 42
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "63c95dbd-76f5-4233-a390-682919809946",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 189,
                "y": 42
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "043b0f30-25e6-45a0-8a17-f41e68ec49b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 200,
                "y": 42
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "4a88bd75-2054-47a4-a3f1-f22ca880b004",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 50,
                "y": 22
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "f8d83ec6-60e0-4e0f-b55f-a957dd5d05aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9eb142f7-7e07-424e-8a1f-56225bf517e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "62d18474-d4cf-4044-a1ed-34f8d66cefa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 38,
                "y": 22
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "5660ecd9-9aa1-40e3-bee9-791ebc44f81f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "51699ddb-aaff-49aa-a554-d4d68299f424",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 22
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "4a0f504c-b345-4ec2-98b7-0c5a67665545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 211,
                "y": 42
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b4d51f6d-a793-41f0-8435-1619eaad31d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 222,
                "y": 42
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "576c86ae-fa69-4187-90ba-2c8667b24d8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6adb20b5-61b3-4fb2-b327-6019dab11cdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "a5a43a05-c812-439e-a18e-27ef7c9e449c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 235,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "c928bd6d-4201-4a5f-ab03-d1ed78d300cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 42,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "d7e2b92f-e6bf-4315-a6f2-5853454ecf50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c77975c5-ba06-49ec-a941-a919675e0ed2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 42
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "9eb80e89-9789-4110-983e-cb91a5e7918a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "05b5b6b5-b551-4393-ba8b-7057f26d8595",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 22
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "67072bb1-fcdd-4cfc-9f19-5b8eda52047e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "399ed372-6636-4166-bf95-9e7f20464d82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 244,
                "y": 22
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "2594d041-9f01-4774-9daf-5e44d219520c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e655d715-35db-4a2f-abb7-16a991756708",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "1ac10c30-342e-4c72-a13f-334157d2b2f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 98,
                "y": 22
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e847038a-69a2-4928-8ff5-edd158816b60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 74,
                "y": 22
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d472731d-83c0-4800-848c-fd67d66c6d08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 86,
                "y": 22
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "239ca595-f3c6-43e9-85f3-810c44785971",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "cabe90e8-f0f2-4805-bad6-a8c92ef7ebbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "bc524bfb-1548-4634-90fe-217d040794b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "95fdf7d4-0249-4cfb-956a-a239a03c3d8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "dc6594cd-6bc1-4643-971e-cca30237145e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 110,
                "y": 22
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f54ff539-b847-4502-b078-eaac4d5fe49c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 162,
                "y": 62
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e8c73d7d-a077-4a03-b6a4-ef7032f53237",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 101,
                "y": 62
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "f387731d-5da0-4d4b-93fa-62adab03c935",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 155,
                "y": 62
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "e5ff9661-d7d1-4000-be54-caec9e4789fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 233,
                "y": 22
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "1a25ce89-7d02-44ce-a1c3-8c68c6febcbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 122,
                "y": 22
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a2319706-83e1-4b26-b946-81804b76c88f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 188,
                "y": 62
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b36af566-0367-475c-b48b-f6c4b4531719",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 211,
                "y": 22
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a88b55b8-c8f2-4cb3-b71b-8cb3a5ee8360",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 200,
                "y": 22
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "0b2604a3-5f99-4a63-bdc9-3b884f98a3e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 189,
                "y": 22
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "756d73c4-a895-40ca-8b4a-e3880c070f96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 178,
                "y": 22
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "050b4a2b-8372-48ab-8c37-983032af2535",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 145,
                "y": 22
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "4d04cd0d-3df4-4a13-8e38-c9144540b9ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 125,
                "y": 62
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "9b1b089e-72b3-462e-b946-486a897650b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 35,
                "y": 42
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "850d4308-a4f3-4d1f-9f99-1abd5311e9a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 32,
                "y": 62
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "f8e16372-12e0-4788-96f7-2a2b1ff7613b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 205,
                "y": 62
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "cb5851d4-13e3-4456-9098-d62fcf24880d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 148,
                "y": 62
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "7aae0fab-df2e-4fcc-a8fa-4d80966bafa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 12,
                "y": 62
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "2c088767-503b-46cc-bef8-7769f995848f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 200,
                "y": 62
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "5a0013d7-4620-4dd1-a2c2-830c229ec4d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "96a8e343-5851-44bc-89fc-2557c035f645",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "88eef4b9-26ce-4ea6-8ac6-80016b459d9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 62,
                "y": 22
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "cc53186a-7838-4c0f-a34a-313480251d01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 134,
                "y": 22
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f858f41b-02da-4ee0-94e1-5832b57bf5b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 24,
                "y": 42
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "078c5cb4-64ac-456c-ac1d-700f16ddd837",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 133,
                "y": 62
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d83952d4-ce8b-430e-8ec0-3ac3d4477780",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "16c760b7-d5b2-438c-a6fd-737d22ac02ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 61,
                "y": 62
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d35aae0a-3284-4a7e-97fd-d514ed80fac9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 22,
                "y": 62
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "5fdaee74-b081-40cf-82c4-9118cbe2151d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 222,
                "y": 22
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "6f69f413-9582-43dd-b4be-9aa58f18f367",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f89f9a86-6a28-4273-a49f-20acaee65705",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 167,
                "y": 22
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "89627b19-cf95-443c-b628-1e3cb82305f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 156,
                "y": 22
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "a41f6267-8bae-45fd-96bd-e713efc4f75c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 244,
                "y": 42
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "84ed0e74-320c-420c-8860-6709f6b1903d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 109,
                "y": 62
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "eaea15ba-536c-45da-a1da-9e3a2964dd70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 230,
                "y": 62
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "780402b4-7889-4997-858b-4ea3a4d02ff4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 117,
                "y": 62
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b104405f-d8ca-435a-9a30-0e1df570b8e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 42
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "6da71ab3-673b-41ee-9145-3c871f26e747",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "702800ff-cc0a-40b8-8101-9a2ac6597c01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "73adf5fd-1668-429f-831a-dd281011e72f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "1c32c875-f911-464b-84fc-30a3522934eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "64561e6e-845d-4114-a26c-fb6514438ad2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "a79f93f9-96b2-4d54-959b-25d088282e4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "14c537d1-901d-4b47-8c92-0333ee51a264",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "84345a8b-d4bb-441e-903a-fb5fef71ba42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "b88786c1-f132-481a-9108-b6565862a0e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "feda6595-ab2e-451b-9481-e7f43b48be8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "473357a7-3a69-4849-ac29-aba8e78ed1af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "e85808f7-1a59-4477-a2c7-af8e6c6c100f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "880f6481-c630-4c36-bc7f-d6de19f2e318",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "b86138cf-b264-49a5-a443-bd0ec241ec95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "8e31928e-91ff-48b2-ae52-f45e06e43d66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "94bd16aa-6ade-448b-bc17-f93a81f438c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "a49f9adb-f41d-4823-ad56-c2a6ddf06d36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "5090f0d2-ac9b-4c14-9db5-a31d124c9852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "ae44c828-3712-4762-80cd-3ab735be2b75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "3f65f7aa-d9c5-4358-83db-a41da84b9e86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "d86bd84c-84db-4b97-8a71-317a90a9f18a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "de710591-e84a-4374-aa9a-d5744a201946",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "a7793c1e-36fa-4315-b376-f8f2759a4d11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "bfce9133-f3f1-4665-89cf-d07a85680e5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "c6027899-7a1a-47ed-a72a-4ac09ef105f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "0207c8e0-4f5a-49cf-9a52-8b4e243ceeb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "f23a9040-cb66-441a-ac47-cf3e5de1656b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "f55eaa29-ba69-4772-ab9a-2e51260b661a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "5a73c49c-ab2a-40c6-8273-a78992e703bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "115d6a3a-ddf2-4141-9a44-ac0c181b8b4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "5a9d8df6-ab63-4457-9e4f-ef61bb50e51b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "d24ec90d-bf8c-41e3-8c27-b51c16695933",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "efafe1a3-e7c7-43f9-a484-4b4e5e3ea497",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "ea0bf76e-be80-49d0-884f-4441657d68e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "137b1728-2883-46bd-8606-ee467e5f0b41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "b9a7c981-7903-4ce6-8038-f76d073c03f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "c4ba7485-f36d-4b93-8343-b5c6cde31ed5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "5221f504-188b-45b9-97e8-9960e840c550",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "d14e4b83-deb6-4d33-92fe-a864a37f52f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "e1d83499-0b37-4d11-b209-b8732c18d6d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "67791e1b-bdfc-42b6-b395-8503b3bc8c9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "1e04cce7-8bd4-4873-93c0-17d4fa9582b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "412e4f0c-2649-4c27-971b-98445e1738a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "0d2f6567-ce5e-4657-82c9-569c027e2033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "413e18e4-d456-4d19-9c60-248736c0e980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "cbf5f94d-af0e-4e03-a72a-35a4cc4491f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "95682e64-5d73-44f6-853a-aa3f767db54c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "e3f5f6a0-f24b-4230-b9bb-fbd09a8bd721",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "b7529c34-57f3-46c5-8239-5890b12e800e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "d319a0bf-087f-4a12-ba30-f1d18ecac6fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "3c188083-02c2-45ad-828c-74b83a3b6045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "1f43e0bb-f0d3-418a-aea0-7d9b3ed9d0cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "3a1aa2c7-76a6-40e7-a963-b9dabe8e9f86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "15ec1416-6618-4514-bebd-59c1976bacea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "5b24d13e-bbf7-44aa-b0c7-9542db83f440",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "baf2e3c6-af22-4dac-97ed-7acc2961588c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "b0dafd4b-0069-4172-baac-40c17005da06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "ae15118d-544e-4b62-a404-8444f26f83f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "41fc5fc2-c03e-4358-a4a2-544f137b4a6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "f7b5a3f8-52d1-4b95-b686-cf41a76eedb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "5ade8c05-e601-4ef3-9e37-0738ecedda96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "6cec6d16-3ebc-4284-b201-52d78997283f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "1153e6e9-aac3-4e87-9a03-054a7fad921a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "b9aade9d-731f-49b8-947b-8c071fb709cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "644dc08d-450e-4dc4-b809-60927139c33b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "29a7343c-aeb7-4fa5-a789-f37af60127a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "f4d9cb16-8af6-4ed7-8e1e-fd1f4566fb01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "87a70497-f5c1-4ef1-a185-a8275780aaf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "9cb803f7-5af1-4a97-8d01-eb630ef4a909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "0e07e450-9e09-44dd-b2b1-94eb667ef9d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}