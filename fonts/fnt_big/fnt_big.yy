{
    "id": "93e25816-7acb-4078-a7a4-c9c96fd1dd50",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_big",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 1,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "650e4dce-a536-47b1-87ad-b51a7bafad18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 27,
                "y": 94
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d6fe0557-e565-4dff-945c-4b9093c2f07a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 75,
                "y": 94
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "7b336930-4486-498a-a050-ac451e960397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 170,
                "y": 71
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "0d73557c-b562-48e5-b4d9-259c6dbecb8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 106,
                "y": 48
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f122330a-7bfb-4827-b285-075caac09175",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "3ccc4382-06da-4230-8ae8-317558589cbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "cc5a1081-2aab-433b-a690-62eb0d5f7b1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "631d2200-cd33-4e4a-a1b5-146eab852642",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 69,
                "y": 94
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "3a2671a7-1dbb-4a56-9d54-65d21546a2a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 62,
                "y": 94
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5a992ba9-670d-4baa-a456-6bd952aa83d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 19,
                "y": 94
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "5e121190-91f4-4b73-8ea0-863c53e5ebf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 94
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "0c288c73-6f98-4959-804a-978376ed8caf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 132,
                "y": 48
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "4215e5b3-841c-4ce8-b561-a4b8533bea93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 109,
                "y": 94
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "2268b6ce-686a-4ad0-96fd-08c5ae7a1a49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 11,
                "y": 94
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "853fc07a-7d9f-477f-b1bb-81e424724dad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 119,
                "y": 94
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "8c5e2442-36b7-400e-a014-f0d3b14d9c99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 180,
                "y": 71
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "98fc31e4-312b-47e1-b1f6-e484d0be6093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 86,
                "y": 71
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "af57d26b-2ce5-4e8c-88d3-d1c9bd1124dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 7,
                "x": 225,
                "y": 71
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "8b6d91b8-c614-4f3b-b13d-888111424678",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 110,
                "y": 71
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "15db697e-c852-44ab-b27d-f1dcd83b883b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 62,
                "y": 71
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "430c2e43-fa01-48b0-ae32-464d420dff45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 145,
                "y": 48
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b2765797-8e5c-4e2d-81c9-e38303e4dd98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 146,
                "y": 71
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "66f623fc-b27c-450e-9557-1545a50c4f39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 158,
                "y": 71
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "530d6586-6c69-4187-b696-64f8bbad6d4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 98,
                "y": 71
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "33378e80-b671-44f9-b607-f765885584a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 134,
                "y": 71
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "58d08274-a10d-438b-a502-523d970e2e1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 122,
                "y": 71
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b329defc-accf-4d7a-8f09-50b1ff5962f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 81,
                "y": 94
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "00d667e9-d641-4485-9cb0-5a46f6aaf9df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 87,
                "y": 94
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b8a03ae5-4a78-4eda-ba12-fbf25ed006f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 119,
                "y": 48
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "419d8f5d-c065-4313-a97c-017bf56b3b28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 80,
                "y": 48
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "0992a6ab-60f6-4b09-908c-c00d2bc7684c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 67,
                "y": 48
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f93775ae-4c58-4194-9e17-3c4d98cebed0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 93,
                "y": 48
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "bf938a53-bfc9-4d58-92a9-509b563bca94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "09804f35-00c3-4112-a612-8d700527ff4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "5db0b573-39b4-48b2-8ed3-e2ff8bb2caa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 88,
                "y": 25
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "cbf530be-1f69-414b-abc6-df476b12dfe2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "ece39a07-4d60-42d8-b186-4cc4ccae3a0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 32,
                "y": 25
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "23397977-b454-4c7e-9b29-cb95da87df7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 184,
                "y": 25
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "255865cd-58dc-491f-888c-bff9e08530f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 158,
                "y": 48
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "8ca840fc-f6fe-4858-a769-e94c229176f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 145,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6436e0c9-ec88-45b2-b6cd-79cae46acd77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 144,
                "y": 25
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e66b1896-cb4a-4a35-b008-cd00e0c30746",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 93,
                "y": 94
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "24c6e1ca-72da-4b81-9403-6038e7d98fcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 74,
                "y": 71
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1d07c03e-3587-4365-9d67-b24fefc76032",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 17,
                "y": 25
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "50d373d0-9d1e-413a-abd8-95b0d8c500ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 28,
                "y": 48
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "dd56b6f1-dbe3-4ffb-90d9-55916b1345c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "db28744e-176e-4b48-90e6-10105bb3554d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 46,
                "y": 25
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "617d60ae-eaa4-4ae8-990c-8679ef82f582",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "75ac3549-d73a-4e95-bcf5-0d1fb4817285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 210,
                "y": 25
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "c8433b4c-7c5e-4df5-b1d3-6ea8c0662565",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "56658f45-6459-4456-8aff-431d3ca23367",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "b03b346a-bd2e-45c9-a48c-21eb3c5b5551",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 60,
                "y": 25
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "a720d151-7552-4c94-88c7-e19656565c2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 74,
                "y": 25
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "bfc55819-5548-47ed-8d72-1e946b119e50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 130,
                "y": 25
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e2b42ae6-9e06-4bc9-8feb-b44c096a734b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": -1,
                "shift": 13,
                "w": 14,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a4b14863-1b64-4d46-be55-42bd1f14a91c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "7552777c-d2bb-4d91-8b19-9b20adf6f952",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "755173ef-e48e-4742-aef4-34d051350ea1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": -1,
                "shift": 13,
                "w": 14,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "541c909b-f3a4-4480-953e-0582262b3cd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 102,
                "y": 25
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "97e6f369-f1a9-4a4b-aebf-2c5fec23efaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 55,
                "y": 94
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "52042ebf-e558-4b7c-9202-87e661e402e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 243,
                "y": 71
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "11a42632-75d3-4387-b4cc-dc5fa7f775ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 48,
                "y": 94
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "635228e7-7e52-467f-a4ab-e80f192852b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 71
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "25811cf0-edb9-479d-b5a0-665dd6240de6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 116,
                "y": 25
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "db77eaa1-75ce-471d-a435-30c7ab9daebc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 41,
                "y": 94
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8d3e0bbd-c5af-45f0-a000-5b222c6f6fbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 218,
                "y": 48
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "41e4f33f-989e-4481-b716-84078dfaa2df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 71
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "80261635-07c4-4579-a9b7-00803f8dac7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 41,
                "y": 48
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ed8eca32-24db-4597-89b9-b8bab45ebb17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 223,
                "y": 25
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8ed6e4c6-bf01-44ae-b0c7-44f6012c5099",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 170,
                "y": 48
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "b487f7b6-223e-48f9-9080-57c757bda98f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 216,
                "y": 71
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "7820f549-085f-429c-b8fc-6bfa866e5446",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 15,
                "y": 48
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "5d1836b0-6051-4479-bac4-307065b81f64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 182,
                "y": 48
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "37af277e-2bec-478b-9b68-52ffe9effee9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 114,
                "y": 94
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "5d56c518-d279-46f3-ad11-19904b194abe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 34,
                "y": 94
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "37be8627-4851-4cae-8855-34a554415491",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 194,
                "y": 48
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e5ce3b42-2220-4d4c-aeb0-0993c780f717",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 104,
                "y": 94
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d359a74d-8539-4456-8e66-26235d989092",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "77ea8421-8031-42ff-bd8e-8cefbd8fffe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 71
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "fb3848da-1557-477c-8c88-7253d8296801",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e65dacf8-ac2c-41d4-bae1-35a46c64f746",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 206,
                "y": 48
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "4881ca1c-25f7-4b7e-ac60-507748aa5223",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 236,
                "y": 25
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c14e91ce-0828-4708-b7ed-bd36d23b1396",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 189,
                "y": 71
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b9aa6e0b-df01-4181-a290-49c001149b0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 230,
                "y": 48
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "886a7c9e-460d-42b2-9936-a2e6ca87423a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 234,
                "y": 71
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3db39917-357a-41fc-8f99-eca826ddda20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 242,
                "y": 48
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "7ea31421-4161-4d6b-aec1-8d89c43543f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 197,
                "y": 25
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "1f56ebfd-7e08-4949-9bd4-558a7eef86d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "bc166166-101d-4440-87af-a09232bd6ecc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 171,
                "y": 25
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "5881ebdf-36b1-4c87-bbd8-7e48964f8661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 158,
                "y": 25
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d9bd8f23-fded-4166-82f2-91822a91ff02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 71
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "83cbcc34-6b8c-4c08-8665-263c54da6df8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 198,
                "y": 71
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "4f951d5d-4f46-4a63-8c9e-dd0cb17642a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 99,
                "y": 94
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "db5324c8-0c55-42b6-940e-2173c4678049",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 207,
                "y": 71
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "853f2038-cc5f-4ee4-a14f-5f819c56cdbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 54,
                "y": 48
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "6a028cc8-08f2-4fd4-b16a-4fa35734f7b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "f09b50bd-27d1-435a-a627-30cc18a77e94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "7d53f9cc-f662-4cff-a423-fb263ee5d46d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "e86ea520-b1a8-4c41-ba04-4936e67e7003",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "e4734e68-1092-413e-ac2b-de48137dafea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "3da30d4b-a0ed-48ab-bbea-1c8f53b21773",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "95e079be-43e7-4fb9-ad23-3905e0e8b8fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "d4bf24ce-8150-47d6-9a1e-83d0348e7d45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "3c54a97f-d6ce-4308-b7b9-c690850ec2ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "45364165-e61b-46b2-a837-6a046f5b0bbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "214b205a-cb8a-47d6-b0fa-fcdc656fcd25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "f55d26c9-e018-4b5a-9fad-a50a99bb6a48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "8d15f1fc-0ffe-4e13-b7fb-9fdfb2d603cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "f361990b-74d9-4d80-a164-92a84eabc9ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "8e719c1c-df1f-4add-8230-7cfa237062d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "f17ba0c2-afc8-43e8-9e34-feb26b5ae2ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "725f92d7-4904-45ae-be79-dc5a9af4f506",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "4b16acf1-18a6-4c5e-9c46-e320851dd244",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "48e9fb94-532f-41aa-acf2-d248f2575f69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "a34181b1-83ab-4630-b458-65e4013e20c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "9c95c3db-f9ad-4a29-b4b3-a1afbaecc87b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "528dc122-76f6-4e28-bb7a-a9b91d2d3fb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "bd62910c-123c-4a0c-9e29-438649c3cd9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "07cb29e4-75e3-47cd-89aa-abd62836bb94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "0de7bc12-875b-487b-8fdb-5f56a7cd9bd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "1fe20852-9ff0-48c7-8113-e8468e655cbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "3dc311da-5d7e-4806-a7c8-f691b45355cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "0c6cb0ca-34c9-4840-ab1f-e42cdc14f49c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "9db6ddca-add9-4e71-a30e-c4dec768e911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "7a3d1598-e972-4b05-ac0b-6b45fcc3b976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "8c37543f-e4b5-4d75-a2b9-f5f93a2b4acf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "86ac422d-5e45-44b9-90c4-3ac582168cf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "32f7dd3a-eceb-4fea-9743-b272d485aaf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "be85d532-a3d3-49a0-aecc-e0ced8d2a98e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "56ed8dcb-f882-495c-9c6b-ccb031632837",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "483210d2-5653-4090-bae9-a0863b211df9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "c7343540-fb59-42f8-8f29-be12b281b15e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "6e364c20-5b35-4e9b-906a-acc27964a858",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "c4a06809-8df8-4ce2-a168-26ee5b8444e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "955fd922-d55f-442d-8adf-f566c250d09a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "630ac2b5-996f-4da0-bf8c-14002f37fb3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "a5e07527-c7a1-41cf-a18e-97a5e49f8687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "8b41bd13-179e-4f20-a2a5-a30819877958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "dfec336f-5e9a-4165-9c1f-1a1796f40a9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "a5ae8653-74fa-4abe-bd75-f9bc6d9adc14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 894
        },
        {
            "id": "35545190-e129-443b-a20e-4315b9ce6cb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "7af89b97-e76e-4f7b-ba23-1092c1ba9a30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "bb8e5e9f-3d30-4ac2-b161-b7a31ad2b651",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "d1adcd5f-52a9-49a8-8575-532082f1013f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "687aa630-7692-4f5f-bd13-b70ca23aeeeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "d38cf162-87f0-443c-95cc-9371a38fb667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "f9b30cfb-ac73-4b80-9eed-f3524834c220",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "8e39db51-89e5-43da-8128-87a7dc6c0156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "b92148ab-31a3-41c8-acbd-78443d85e68e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "c2ef83e0-c11f-4b9d-917e-6c410dfc8a1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "a560509b-a465-4441-915c-f666c1201ebd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "b2d217db-29a0-45fa-9955-c521ad98afc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "32aea198-0e76-42ed-8438-31205aeb9759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "36099793-81b5-4ef0-8ce7-6dabcfd450bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "2ea79bf1-ead2-4b65-8a11-3d226aa30f61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "2c9ac483-6d78-4f4d-85c4-c685cf70c232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "b57de89c-9979-4902-8306-ed1f13e1fb3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "a2877d72-2b62-4838-99be-b7694754366a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "4ae3c5ac-03cc-4066-a856-f33c256b1b27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "f17f8fa6-d918-4829-a9b8-56dfad23127b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "627ab867-b011-4585-a3dd-235eea779c02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "fe623d3e-b694-4361-bf8d-b9cc35512205",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "31bfdc4b-e45c-4118-93a7-7e68e989402a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "83b5f406-0cbd-4497-ac99-b2830cc27c9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "22727246-643d-40a1-aef6-b5cb2593bdfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "d657a6db-92b8-48a8-b174-ea58ba7c9aae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "7f24eef4-3bf6-421e-8744-acf8748bbf90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "f7d46bac-5898-4efd-bc9c-6efc6f86ed21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "176f514c-178e-458e-8ec8-6191e5c31e9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "1f10ece4-6e8c-4f81-8c91-c264f17a32fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "1cbda2f7-4190-4dab-bb2b-a437dd955195",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "18491d71-1c00-49da-a190-47b03c89698a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "c0b24f49-8bb2-43fd-9556-8fe836b89d96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "46f5a75e-218b-4cee-acda-b2b839a7dabf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "82746e45-f749-4dbd-b1b0-735cd903d8a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "0a75745c-09b6-4aa4-86a2-48c901fef036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "06c81652-0986-4cc3-a22e-493fb23c74fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "6a6a2c58-75c0-4cd2-b25a-eb2d0416bc4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "bcea6db5-c812-4c15-a4e1-863bdbceef53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "133a46ef-93f6-43e5-8b83-c8f8029a67f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "cf3f0a02-b355-4bce-a3f0-b9b0758ed6e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "0cd4d83d-9d7c-4acd-842b-d8002290faa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "5eebd83a-488e-46ec-a40c-0b655bda852e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 14,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}