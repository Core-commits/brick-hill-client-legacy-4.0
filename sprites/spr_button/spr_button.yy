{
    "id": "9a976903-6d5d-4892-9cbc-d0390d11c721",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 83,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "850d8d34-9d60-4d2f-b575-8a867025a223",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a976903-6d5d-4892-9cbc-d0390d11c721",
            "compositeImage": {
                "id": "ea52cb68-262f-4d47-b608-fc460f1a89cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "850d8d34-9d60-4d2f-b575-8a867025a223",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ec0175e-fd0d-4baf-b05d-b58f199126fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "850d8d34-9d60-4d2f-b575-8a867025a223",
                    "LayerId": "7d05f311-7f80-480c-bf8b-11c83dc8e35b"
                }
            ]
        },
        {
            "id": "4622f09b-b536-4eb2-b278-9ad8fc5d923f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a976903-6d5d-4892-9cbc-d0390d11c721",
            "compositeImage": {
                "id": "d7d3ed04-431f-4cb5-9c03-87277bc9d300",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4622f09b-b536-4eb2-b278-9ad8fc5d923f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47f4a8b9-40ed-42e6-95b8-c54313a8c10f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4622f09b-b536-4eb2-b278-9ad8fc5d923f",
                    "LayerId": "7d05f311-7f80-480c-bf8b-11c83dc8e35b"
                }
            ]
        },
        {
            "id": "c5a99f58-71fb-463e-af5c-45e32d05b64e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a976903-6d5d-4892-9cbc-d0390d11c721",
            "compositeImage": {
                "id": "38bfa602-774b-4651-96b3-58a5a22f3d7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5a99f58-71fb-463e-af5c-45e32d05b64e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30ff89d2-4812-45f4-99a1-062cfd2a951e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5a99f58-71fb-463e-af5c-45e32d05b64e",
                    "LayerId": "7d05f311-7f80-480c-bf8b-11c83dc8e35b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7d05f311-7f80-480c-bf8b-11c83dc8e35b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a976903-6d5d-4892-9cbc-d0390d11c721",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": 0,
    "yorig": 0
}