{
    "id": "f8ba0bb2-b20a-4d3e-bbda-382ca4413f06",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_vap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92c27df8-04e3-4223-ab57-a35b72554347",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba0bb2-b20a-4d3e-bbda-382ca4413f06",
            "compositeImage": {
                "id": "2abe0820-2e64-43f8-aefb-6a811deb824a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92c27df8-04e3-4223-ab57-a35b72554347",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ad8bd67-a7ef-42d6-ba48-65820015eb17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92c27df8-04e3-4223-ab57-a35b72554347",
                    "LayerId": "29bef25d-ab57-4ac6-90ad-e9f52e9259be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "29bef25d-ab57-4ac6-90ad-e9f52e9259be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8ba0bb2-b20a-4d3e-bbda-382ca4413f06",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 2
}