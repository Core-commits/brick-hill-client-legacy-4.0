{
    "id": "8a2101ff-4060-48d8-8133-acdc4ba6c85f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bkg_snow",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 399,
    "bbox_left": 0,
    "bbox_right": 399,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "486be37f-f793-4bf2-a810-2216a0aa19f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a2101ff-4060-48d8-8133-acdc4ba6c85f",
            "compositeImage": {
                "id": "98b6e835-231b-4fbf-b5de-8784c05607fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "486be37f-f793-4bf2-a810-2216a0aa19f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2780ae90-4496-4106-b486-41f503bae031",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "486be37f-f793-4bf2-a810-2216a0aa19f0",
                    "LayerId": "467a650f-cd0b-4336-be79-c0627b7fb978"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "467a650f-cd0b-4336-be79-c0627b7fb978",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a2101ff-4060-48d8-8133-acdc4ba6c85f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}