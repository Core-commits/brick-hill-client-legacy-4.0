{
    "id": "f1420c24-1c0b-492c-a73b-bd1684e1e01a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_speech",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ce408967-45fe-4721-b9eb-cd3ac58b8db5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1420c24-1c0b-492c-a73b-bd1684e1e01a",
            "compositeImage": {
                "id": "a009f69b-a8e4-4de2-9854-2bacb30db942",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce408967-45fe-4721-b9eb-cd3ac58b8db5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cf6c1a6-5600-44de-97ec-8992e14b1f3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce408967-45fe-4721-b9eb-cd3ac58b8db5",
                    "LayerId": "99223071-ba05-4187-8c19-85239c03a852"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "99223071-ba05-4187-8c19-85239c03a852",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1420c24-1c0b-492c-a73b-bd1684e1e01a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 10,
    "yorig": 0
}