{
    "id": "d10f48d1-b6be-48fb-833a-5e79a67979e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bkg_spawnpoint",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0109bd99-3877-4dd2-ba92-f089c8ff5acf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d10f48d1-b6be-48fb-833a-5e79a67979e5",
            "compositeImage": {
                "id": "5a35928b-0d2d-441a-aa90-139d3875af44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0109bd99-3877-4dd2-ba92-f089c8ff5acf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6df50865-7066-42e1-bbe7-46b0241f0725",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0109bd99-3877-4dd2-ba92-f089c8ff5acf",
                    "LayerId": "2e9d4044-a87f-401c-8ee2-9bad0f9d2e61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "2e9d4044-a87f-401c-8ee2-9bad0f9d2e61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d10f48d1-b6be-48fb-833a-5e79a67979e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}