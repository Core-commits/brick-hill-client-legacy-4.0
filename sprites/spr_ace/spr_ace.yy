{
    "id": "3aec6ebf-0238-46f3-9b95-be0dc7b0835b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ace",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bf16879-9ec6-44c6-bc80-88beedd61034",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3aec6ebf-0238-46f3-9b95-be0dc7b0835b",
            "compositeImage": {
                "id": "d6e0de86-0bc7-4e54-8b28-5be0d72b39d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bf16879-9ec6-44c6-bc80-88beedd61034",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08173132-b5c3-4bb7-b12c-983e9d90c471",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bf16879-9ec6-44c6-bc80-88beedd61034",
                    "LayerId": "8eca3797-39d7-48fd-8c51-7a447c4b2751"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "8eca3797-39d7-48fd-8c51-7a447c4b2751",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3aec6ebf-0238-46f3-9b95-be0dc7b0835b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 2
}