{
    "id": "664d6fa5-4d42-4529-b8bd-9e4eb8a6bb68",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bkg_message",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24b6a695-ab1c-482c-866e-d0ae0b4dd076",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "664d6fa5-4d42-4529-b8bd-9e4eb8a6bb68",
            "compositeImage": {
                "id": "b1db8f06-77ad-4f11-a67a-240b65d8f35c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24b6a695-ab1c-482c-866e-d0ae0b4dd076",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bbffca6-acc2-49a8-a847-a1db5f7cb8ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24b6a695-ab1c-482c-866e-d0ae0b4dd076",
                    "LayerId": "bd754cd2-e0a4-49f5-84e3-79d48f526458"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "bd754cd2-e0a4-49f5-84e3-79d48f526458",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "664d6fa5-4d42-4529-b8bd-9e4eb8a6bb68",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}