{
    "id": "ad59816b-d718-443d-8993-c9cd3841ab98",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_check",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43b9a179-fe84-4b9d-a24e-84a2e8ee68c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad59816b-d718-443d-8993-c9cd3841ab98",
            "compositeImage": {
                "id": "f8197ffd-d868-4213-b535-7536fbbd8776",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43b9a179-fe84-4b9d-a24e-84a2e8ee68c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69021801-648f-49a7-862b-f0ace91138b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43b9a179-fe84-4b9d-a24e-84a2e8ee68c8",
                    "LayerId": "232f5b9e-1d73-4d5c-ae87-112d0d5133bd"
                }
            ]
        },
        {
            "id": "8b4220c0-74b6-441a-83b1-68778aa459fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad59816b-d718-443d-8993-c9cd3841ab98",
            "compositeImage": {
                "id": "3a378ae5-16eb-4abd-bfd5-32f9c707e38d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b4220c0-74b6-441a-83b1-68778aa459fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "199c07e2-91ef-4b83-9f30-0048fa5ba928",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b4220c0-74b6-441a-83b1-68778aa459fe",
                    "LayerId": "232f5b9e-1d73-4d5c-ae87-112d0d5133bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "232f5b9e-1d73-4d5c-ae87-112d0d5133bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad59816b-d718-443d-8993-c9cd3841ab98",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}