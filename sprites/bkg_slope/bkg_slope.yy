{
    "id": "eb18ed5e-8de3-464c-a3a1-8df0ad66a783",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bkg_slope",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed28a2be-d1fc-4674-99d2-8f5d30b3fcb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb18ed5e-8de3-464c-a3a1-8df0ad66a783",
            "compositeImage": {
                "id": "e24b0f82-335d-48bd-b319-8cada1378fc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed28a2be-d1fc-4674-99d2-8f5d30b3fcb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c2f6a08-090f-4999-90ff-18ff3c7abc22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed28a2be-d1fc-4674-99d2-8f5d30b3fcb6",
                    "LayerId": "884ddb59-9cff-471e-83f4-dab7ca828030"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "884ddb59-9cff-471e-83f4-dab7ca828030",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb18ed5e-8de3-464c-a3a1-8df0ad66a783",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}