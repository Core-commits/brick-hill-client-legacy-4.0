{
    "id": "3aa148a5-7634-49f2-be0f-8661326a0b7e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bkg_face",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 311,
    "bbox_left": 144,
    "bbox_right": 274,
    "bbox_top": 117,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19b8a4b4-4a52-49e1-ad5c-920d4de40d6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3aa148a5-7634-49f2-be0f-8661326a0b7e",
            "compositeImage": {
                "id": "a1b73072-5976-432e-bc64-1cbad08b2c4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19b8a4b4-4a52-49e1-ad5c-920d4de40d6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd7e7c52-bd45-438b-8cb0-08c716f26a6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19b8a4b4-4a52-49e1-ad5c-920d4de40d6d",
                    "LayerId": "574da53d-7b2a-403b-8a45-14a1a6fe7c73"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 420,
    "layers": [
        {
            "id": "574da53d-7b2a-403b-8a45-14a1a6fe7c73",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3aa148a5-7634-49f2-be0f-8661326a0b7e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 420,
    "xorig": 210,
    "yorig": 217
}