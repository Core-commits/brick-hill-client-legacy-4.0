{
    "id": "19b33a99-7b45-4f3c-80de-7b3ee6f600c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_admin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13a0884f-32f8-42ec-8441-a61dd5e461e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19b33a99-7b45-4f3c-80de-7b3ee6f600c3",
            "compositeImage": {
                "id": "cae46d5d-a44e-4e48-8170-09400d37f264",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13a0884f-32f8-42ec-8441-a61dd5e461e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b90d93c7-f070-49c3-bc7d-60c0e7aa3808",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13a0884f-32f8-42ec-8441-a61dd5e461e5",
                    "LayerId": "be04020b-7093-4956-942e-83e825d8d473"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "be04020b-7093-4956-942e-83e825d8d473",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19b33a99-7b45-4f3c-80de-7b3ee6f600c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 2
}