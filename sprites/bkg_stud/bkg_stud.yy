{
    "id": "00dfba44-53db-4f60-bcf7-b0e19685af39",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bkg_stud",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b950e94-7bfe-4506-8793-457e6bfd60db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00dfba44-53db-4f60-bcf7-b0e19685af39",
            "compositeImage": {
                "id": "d9d94a28-cc59-4abf-9932-3050e60aea14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b950e94-7bfe-4506-8793-457e6bfd60db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56e81b90-1646-4090-a29f-3a87bead3581",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b950e94-7bfe-4506-8793-457e6bfd60db",
                    "LayerId": "2b3ba28f-d709-40b8-8da7-f9cd21301218"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "2b3ba28f-d709-40b8-8da7-f9cd21301218",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00dfba44-53db-4f60-bcf7-b0e19685af39",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}