{
    "id": "de31997e-1d1f-4eac-ac44-6671fd14e65e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bkg_snow_drop",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 181,
    "bbox_left": 7,
    "bbox_right": 183,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d737031-b335-4b26-8b71-19427866502f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de31997e-1d1f-4eac-ac44-6671fd14e65e",
            "compositeImage": {
                "id": "f3646a0b-164f-4837-ad06-665ffad813ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d737031-b335-4b26-8b71-19427866502f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "051de20e-b250-4176-9572-8f9739ae99f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d737031-b335-4b26-8b71-19427866502f",
                    "LayerId": "5a20899a-ed2f-4999-b6c5-e9fb55e2557c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "5a20899a-ed2f-4999-b6c5-e9fb55e2557c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de31997e-1d1f-4eac-ac44-6671fd14e65e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}