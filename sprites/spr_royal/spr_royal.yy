{
    "id": "f4eeeb43-46fe-43b5-96bd-6299545bf195",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_royal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc813149-eb9f-44fd-ad08-2d12c6f2e0dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4eeeb43-46fe-43b5-96bd-6299545bf195",
            "compositeImage": {
                "id": "4cce4d18-37bb-4935-8005-e092fb47cc30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc813149-eb9f-44fd-ad08-2d12c6f2e0dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5224298-2011-44ab-aefe-1845eb018639",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc813149-eb9f-44fd-ad08-2d12c6f2e0dd",
                    "LayerId": "f41f8424-3d1e-49d5-9804-4ab3b5e8db83"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "f41f8424-3d1e-49d5-9804-4ab3b5e8db83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4eeeb43-46fe-43b5-96bd-6299545bf195",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 2
}