{
    "id": "f4ffa1d2-4801-4421-a5e4-57a4c99789f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mint",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e1519c7-5c45-491f-acc7-dd0cfab2dab5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ffa1d2-4801-4421-a5e4-57a4c99789f1",
            "compositeImage": {
                "id": "35494fb4-4036-44d0-b839-a67786e8a91b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e1519c7-5c45-491f-acc7-dd0cfab2dab5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e645fd7e-9149-42a7-8921-66d74228ba97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e1519c7-5c45-491f-acc7-dd0cfab2dab5",
                    "LayerId": "c5d83b6b-6206-47ee-80f6-5d83486f3238"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "c5d83b6b-6206-47ee-80f6-5d83486f3238",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4ffa1d2-4801-4421-a5e4-57a4c99789f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 2
}