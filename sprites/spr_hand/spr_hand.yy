{
    "id": "81fbbec9-d11b-481c-961a-1a97be95d50e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "625abdc3-ecf9-4b85-955e-42bfb98d8fa0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81fbbec9-d11b-481c-961a-1a97be95d50e",
            "compositeImage": {
                "id": "8c98113d-a48c-45e2-a4d7-c5fb16d37554",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "625abdc3-ecf9-4b85-955e-42bfb98d8fa0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a62607a3-6cfc-441d-80a4-2dac412ed5ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "625abdc3-ecf9-4b85-955e-42bfb98d8fa0",
                    "LayerId": "2f6d0e5f-8fe7-48e3-90c2-1014a8ae4e56"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2f6d0e5f-8fe7-48e3-90c2-1014a8ae4e56",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81fbbec9-d11b-481c-961a-1a97be95d50e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 21,
    "xorig": 10,
    "yorig": 16
}