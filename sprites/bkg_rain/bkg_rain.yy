{
    "id": "187fd1b6-af8a-4183-99e9-1f7030c98264",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bkg_rain",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 180,
    "bbox_left": 25,
    "bbox_right": 186,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fba94d0f-edec-4510-927c-a56b5517dba4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "187fd1b6-af8a-4183-99e9-1f7030c98264",
            "compositeImage": {
                "id": "ab626dda-6bf6-4bdb-9fca-d0ab7efb5146",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fba94d0f-edec-4510-927c-a56b5517dba4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7e47acf-875d-41d0-9296-72e750591ef3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fba94d0f-edec-4510-927c-a56b5517dba4",
                    "LayerId": "db0d1f79-22d8-4f2d-b827-300f7d96ff33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "db0d1f79-22d8-4f2d-b827-300f7d96ff33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "187fd1b6-af8a-4183-99e9-1f7030c98264",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}