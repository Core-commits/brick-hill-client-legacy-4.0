{
    "id": "3dea12d3-3675-4347-a3b0-1ed13be6dcc1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bkg_stud_under",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7fdad1c4-c923-4de0-8718-793a1e3a2251",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dea12d3-3675-4347-a3b0-1ed13be6dcc1",
            "compositeImage": {
                "id": "bbf45bf3-efc5-4779-aecd-fb67e92e5f3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fdad1c4-c923-4de0-8718-793a1e3a2251",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "159ee51b-39a8-4590-912a-e2210a5405b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fdad1c4-c923-4de0-8718-793a1e3a2251",
                    "LayerId": "dcd96ffd-dc60-4041-b31d-ced5f8fdf123"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "dcd96ffd-dc60-4041-b31d-ced5f8fdf123",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3dea12d3-3675-4347-a3b0-1ed13be6dcc1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}