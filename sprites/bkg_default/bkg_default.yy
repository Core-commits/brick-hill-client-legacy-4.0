{
    "id": "9599e05c-7892-467e-99b0-1a1002c4ee10",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bkg_default",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 626,
    "bbox_left": 325,
    "bbox_right": 638,
    "bbox_top": 46,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54171890-d2c3-49ac-9a4e-83565072c44d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9599e05c-7892-467e-99b0-1a1002c4ee10",
            "compositeImage": {
                "id": "4762822b-55d4-486a-92ac-1f07543f9b68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54171890-d2c3-49ac-9a4e-83565072c44d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a537697e-c645-4e33-a7d4-e4495272ae2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54171890-d2c3-49ac-9a4e-83565072c44d",
                    "LayerId": "9813df08-0354-4629-9946-41a0d4925599"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "9813df08-0354-4629-9946-41a0d4925599",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9599e05c-7892-467e-99b0-1a1002c4ee10",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}