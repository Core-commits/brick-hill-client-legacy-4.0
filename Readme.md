## Hello!

Hello, this is Shiggy (Aka Edge.). I've been for a while porting the current client to the updated version of gamemaker studio.
GM2 in this case.

I wanted to share this with the current developers, feel free to take a look or make any fixes you feel like.

This project is currently maintaned by me and flycatcher. 

Flycatcher rep is pretty bad tough, therefore i did not include him in the member list.


## Objectives:

- [ ] Update Functions [WIP]
- [ ] Update Buffers [WIP]
- [ ] Update HTTP requests. [WIP]
- [ ] Clean Up the code
- [ ] Final Retouches

## Extra

Gamemaker Studio 2 Documentation:

https://docs2.yoyogames.com/

---

If you want any extra member in just make a comment. (They must have a gitlab account)

