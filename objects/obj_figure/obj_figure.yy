{
    "id": "0b4bf8a0-88ee-460c-8ea6-c7e4845d6606",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_figure",
    "eventList": [
        {
            "id": "696bd8d4-abd2-4709-a536-b319c4d61ddf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0b4bf8a0-88ee-460c-8ea6-c7e4845d6606"
        },
        {
            "id": "aa3df60d-421f-4d0c-82d1-679272a95028",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "0b4bf8a0-88ee-460c-8ea6-c7e4845d6606"
        },
        {
            "id": "9ffb98e8-9dcb-4137-b8ab-0d382aa6f69c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0b4bf8a0-88ee-460c-8ea6-c7e4845d6606"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}