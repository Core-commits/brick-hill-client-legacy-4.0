xPos=GmnBodyGetPosition(body,0);
yPos=GmnBodyGetPosition(body,1);
zPos=GmnBodyGetPosition(body,2);

xRot=radtodeg(GmnBodyGetRotation(body,0));
yRot=radtodeg(GmnBodyGetRotation(body,1));
zRot=radtodeg(GmnBodyGetRotation(body,2));

if instance_number(obj_asset_download) > 0 {
    if TexDownload != -1 && instance_exists(TexDownload) {
        if TexDownload.result {
            Tex = background_add(TexDownload.file,0,0); 
            TexDownload = -1;
        }
    }
    if ModDownload != -1 && instance_exists(ModDownload) {
        if ModDownload.result {
            Model = scr_load_model_obj(ModDownload.file, ModDownload.assetId);
            ModDownload = -1
        }
    }
}

