/*if current_time mod 60000 == 0 {
    post_playing();
}*/


// THESE ARE CACHED !!!!!!!
if instance_number(obj_asset_download) > 0 {
    if FaceDownload != -1 && instance_exists(FaceDownload) {
        if FaceDownload.result {
            Face = background_add(FaceDownload.file, 1, 0);
            FaceDownload = -1
        }
    }
    if Hat1TexDownload != -1 && instance_exists(Hat1TexDownload) {
        if Hat1TexDownload.result {
            Hat1_Tex = background_add(Hat1TexDownload.file, 0, 0);
            Hat1TexDownload = -1
        }
    }
    if Hat1ModDownload != -1 && instance_exists(Hat1ModDownload) {
        if Hat1ModDownload.result {
            Hat1 = scr_load_model_obj(Hat1ModDownload.file, Hat1ModDownload.assetId);
            Hat1ModDownload = -1
        }
    }
    if Hat2TexDownload != -1 && instance_exists(Hat2TexDownload) {
        if Hat2TexDownload.result {
            Hat2_Tex = background_add(Hat2TexDownload.file, 0, 0); 
            Hat2TexDownload = -1
        }
    }
    if Hat2ModDownload != -1 && instance_exists(Hat2ModDownload) {
        if Hat2ModDownload.result {
            Hat2 = scr_load_model_obj(Hat2ModDownload.file, Hat2ModDownload.assetId);
            Hat2ModDownload = -1
        }
    }
    if Hat3TexDownload != -1 && instance_exists(Hat3TexDownload) {
        if Hat3TexDownload.result {
            Hat3_Tex = background_add(Hat3TexDownload.file, 0, 0);
            Hat3TexDownload = -1
        }
    }
    if Hat3ModDownload != -1 && instance_exists(Hat3ModDownload) {
        if Hat3ModDownload.result {
            Hat3 = scr_load_model_obj(Hat3ModDownload.file, Hat3ModDownload.assetId);
            Hat3ModDownload = -1
        }
    }
    if Item_TexDownload != -1 && instance_exists(Item_TexDownload) {
        if Item_TexDownload.result {
            Item_Tex = background_add(Item_TexDownload.file,0,0);
            Item_TexDownload = -1
        }
    }
    if Item_ModDownload != -1 && instance_exists(Item_ModDownload) {
        if Item_ModDownload.result {
            Item = scr_load_model_obj(Item_ModDownload.file, Item_ModDownload.assetId);
            Item_ModDownload = -1
        }
    }
}

xPrev = xPos;
yPrev = yPos;
zPrev = zPos;
zRPRev = zRot;

/*
lAtPrev = lookingAt
lXPrev = lookingAtXPos;
lYPrev = lookingAtYPos;
lZPrev = lookingAtZPos;
*/

rayCast();

with obj_brick {
    hovered = false
    if clickable == 1 {
        if power(x-obj_client.xPos,2)+power(y-obj_client.yPos,2)+power(z-obj_client.zPos,2) <= clickDist {
            if(obj_client.lookingAtXPos >= x && obj_client.lookingAtXPos <= x+xs) {
                if(obj_client.lookingAtYPos >= y && obj_client.lookingAtYPos <= y+ys) {
                    if(obj_client.lookingAtZPos >= z && obj_client.lookingAtZPos <= z+zs) {
                        hovered = true
                        if (mouse_check_button_pressed(mb_left)) {
                            // Player has clicked on the brick
                            buffer_delete(global.BUFFER);
                            buffer_write(global.BUFFER,buffer_u8, 5);
                            buffer_write(global.BUFFER, buffer_u32, floor(brickID));
                            network_send_raw(obj_client.SOCKET, global.BUFFER, global.BUFFER);
                        }
                    }
                }
            }
        }
    }
}

//set size
if (room = rm_main) {
    if (window_get_width() != global.prevwidth || window_get_height() != global.prevheight) {
        global.winwidth = max(minwidth,window_get_width())
        global.winheight = median(minheight,window_get_height(),display_get_height()-8)
        
        room_goto(rm_size);
    } else {
        global.prevwidth = window_get_width();
        global.prevheight = window_get_height();
    }
}
//

socket_update_read(SOCKET);

while socket_read_message(SOCKET, global.BUFFER) {
    buffer_decompress(global.BUFFER);
    type = buffer_read(global.BUFFER, buffer_u8);
    switch(type) {
        case 1:
            net_id = buffer_read(global.BUFFER, buffer_u32);
            brickCount = buffer_read(global.BUFFER, buffer_u32);
            user_id = buffer_read(global.BUFFER, buffer_u32);
            name = buffer_read(global.BUFFER, buffer_string)
            admin = buffer_read(global.BUFFER, buffer_u8)
            membership = buffer_read(global.BUFFER, buffer_u8)
            authenticated = true
            if brickCount == 0 { // There are no bricks to load.
                bricksDownloaded = true
            }
            break;
        case 2:
            var brkData;
            brkData = ""
            // I fucking hate shit maker
            while !buffer_at_end(global.BUFFER) {
                brkData += buffer_read(global.BUFFER, buffer_string);
            }
            
            load_bricks(brkData);
            bricksDownloaded = true
            break;
        case 3:
            var fig, playerAmount;
            playerAmount = buffer_read(global.BUFFER, buffer_u8)
            var i;
            for (i = 0; i < playerAmount; i+=1) {
                fig = instance_create(0, 0, obj_figure);
                fig.net_id = buffer_read(global.BUFFER, buffer_u32);
                fig.name = buffer_read(global.BUFFER, buffer_string);
                fig.user_id = buffer_read(global.BUFFER, buffer_u32);
                fig.admin = buffer_read(global.BUFFER, buffer_u8);
                fig.membership = buffer_read(global.BUFFER, buffer_u8);
            }
            break;
        case 4:
            //var information
            var user_net_id, id_string;
            user_net_id = buffer_read(global.BUFFER, buffer_u32);
            id_string = buffer_read(global.BUFFER, buffer_string);
            if(user_net_id != net_id) {
                with obj_figure {
                    if(user_net_id == net_id) {
                        packet_handler(id_string);
                    }
                }
            } else {
                //prev_check();
                packet_handler(id_string);
            }
            break;
        case 5:
            var user_net_id;
            user_net_id = buffer_read(global.BUFFER, buffer_u32);
            with obj_figure {
                if(user_net_id = net_id) {
                    instance_destroy();
                }
            }
            break;
        case 6:
            //chat message
            var message;
            message = buffer_read(global.BUFFER, buffer_string);
            messageAdd(message);
            break;
        case 7:
            //gui message/toggle settings
            var type;
            type = buffer_read(global.BUFFER, buffer_string);
            switch type {
                case "topPrint":
                    var message,time;
                    message = buffer_read(global.BUFFER, buffer_string);
                    time = buffer_read(global.BUFFER, buffer_u32);
                    clientTopPrint(message,time);
                    break;
                case "centerPrint":
                    var message,time;
                    message = buffer_read(global.BUFFER, buffer_string);
                    time = buffer_read(global.BUFFER, buffer_u32);
                    clientCenterPrint(message,time);
                    break;
                case "bottomPrint":
                    var message,time;
                    message = buffer_read(global.BUFFER, buffer_string);
                    time = buffer_read(global.BUFFER, buffer_u32);
                    clientBottomPrint(message,time);
                    break;
                case "Ambient":
                    Ambient = buffer_read(global.BUFFER, buffer_u32);
                    break;
                case "Sky":
                    SkyColor = buffer_read(global.BUFFER, buffer_u32);
                    break;
                case "BaseCol":
                    BasePlateColor = buffer_read(global.BUFFER, buffer_u32);
                    break;
                case "BaseSize":
                    BasePlateSize = buffer_read(global.BUFFER, buffer_u32);
                    GmnDestroyBody(global.set,Ground);
                    GroundCol = GmnCreateBox(global.set,BasePlateSize,BasePlateSize,1,0,0,0);
                    Ground = GmnCreateBody(global.set,GroundCol);
                    GmnReleaseCollision(global.set,GroundCol);
                    GmnBodySetMassMatrix(Ground,0,0,0,0);
                    GmnBodySetPosition(Ground,0,0,-0.5);
                    break;
                case "Sun":
                    SunIntensity = buffer_read(global.BUFFER, buffer_u32);
                    break;
                case "kick":
                    // it doesn't matter if you remove this, node-hill automatically destroys your socket yeet
                    show_message("get rekt bitch");
                    game_end();
                    exit;
                    break;
                case "prompt":
                    var msg;
                    msg = buffer_read(global.BUFFER, buffer_string);
                    show_message(msg)
                    break
                case "WeatherSnow":
                    Weather = "snow";
                    break;
                case "WeatherRain":
                    Weather = "rain";
                    break;
                case "WeatherSun":
                    Weather = "sun";
                    break;
            }
            break;
        case 8:
            var user_net_id,dead;
            user_net_id = buffer_read(global.BUFFER, buffer_f32);
            if(user_net_id != net_id) {
                with obj_figure {
                    if(user_net_id == net_id) {
                        alive = !buffer_read(global.BUFFER, buffer_u8);
                    }
                }
            } else {
                alive = !buffer_read(global.BUFFER, buffer_u8);
            }
            break;
        case 9:
            //got brick variables
            //brickID
            var brick_id,type;
            brick_id = buffer_read(global.BUFFER, buffer_u32);
            type = buffer_read(global.BUFFER, buffer_string);
            
            with obj_brick {
                if brickID == brick_id {
                    switch type {
                        case "pos":
                            x = buffer_read(global.BUFFER, buffer_f32);
                            y = buffer_read(global.BUFFER, buffer_f32);
                            z = buffer_read(global.BUFFER, buffer_f32);
                            GmnBodySetPosition(body,x,y,z);
                            break;
                        case "rot":
                            rotation = buffer_read(global.BUFFER, buffer_u32);
                            break;
                        case "scale":
                            xs = buffer_read(global.BUFFER, buffer_f32);
                            ys = buffer_read(global.BUFFER, buffer_f32);
                            zs = buffer_read(global.BUFFER, buffer_f32);
                            GmnDestroyBody(global.scale, body);
                            bound = GmnCreateBox(global.set,xs,ys,zs,xs/2,ys/2,zs/2);
                            body = GmnCreateBody(global.set,bound);
                            GmnBodySetPosition(body,x,y,z);
                            break;
                        case "kill":
                            GmnBodySetAutoMassMatrix(body,2,0);
                            break;
                        case "destroy":
                            GmnDestroyBody(global.set, body);
                            instance_destroy();
                            break;
                        case "col":
                            color = buffer_read(global.BUFFER, buffer_u32);
                            break;
                        case "alpha":
                            alpha = buffer_read(global.BUFFER, buffer_f32);
                            break;
                        case "lightcol":
                            light_color = buffer_read(global.BUFFER, buffer_u32);
                            break;
                        case "lightrange":
                            light_range = buffer_read(global.BUFFER, buffer_u32);
                            break;
                        case "model":
                            model = buffer_read(global.BUFFER, buffer_u32);
                            TexDownload = fetch_asset(model, "brick_tex", "png", false)
                            ModDownload = fetch_asset(model, "brick_mod", "obj", false)
                            break;
                        case "collide":
                            var collide;
                            collide = buffer_read(global.BUFFER, buffer_u8);
                            if(!collide) {
                                GmnDestroyBody(global.set,body);
                            } else {
                                bound = GmnCreateBox(global.set,xs,ys,zs,xs/2,ys/2,zs/2);
                                body = GmnCreateBody(global.set,bound);
                                GmnBodySetPosition(body,x,y,z);
                            }
                            break;
                        case "clickable":
                            clickable = buffer_read(global.BUFFER, buffer_u8);
                            clickDist = buffer_read(global.BUFFER, buffer_u32);
                            break;
                    }
                }
            }
            break;
        case 10:
            var t;
            t = instance_create(0,0,obj_team);
            t.teamID = buffer_read(global.BUFFER, buffer_u32);
            t.name = buffer_read(global.BUFFER, buffer_string);
            t.color = buffer_read(global.BUFFER, buffer_u32);
            break;
        case 11:
            var s, action, slotId, toolName, toolModel, class;
            
            action = buffer_read(global.BUFFER, buffer_u8);
            
            slotId = buffer_read(global.BUFFER, buffer_u32);
            toolName = buffer_read_string(global.BUFFER);
            toolModel = buffer_read(global.BUFFER, buffer_u32);
            
            class = id;
            
            if action {
                s = instance_create(0, 0, obj_slot);
                s.slotID = slotId
                s.name = toolName
                s.Model = toolModel
            } else {
                with obj_slot {
                    if slotID == slotId {
                        instance_destroy();
                    }
                }
                with obj_figure {
                    if (Arm = slotId) {
                        Arm = -1;
                        Item = -1;
                    }
                }
                if Arm == slotId  {
                    Arm = -1;
                    Item = -1;
                }
            }
            
            break;
        case 12:
            var figure_id,figure,id_string;
            
            figure_id = buffer_read(global.BUFFER, buffer_u32);
            figure = -1;
            
            with obj_dummy {
                if(figureID == figure_id) {
                    figure = id;
                    break;
                }
            }
            
            if(figure == -1) {
                figure = instance_create(0,0,obj_dummy);
                figure.figureID = figure_id;
            }
            
            id_string = buffer_read(global.BUFFER, buffer_string);
            
            
            // KEEP THIS CONTAINED IN "WITH FIGURE" UNLESS YOU WANT BANGLA THINGS TO HAPPEN
            // 👀👀👀👀👀👀👀👀👀👀👀
            
            with figure {
            
                for(i = 1; i <= string_length(id_string); i += 1) {
                    switch string_char_at(id_string,i) {
                        case "A":
                            Name = buffer_read(global.BUFFER, buffer_string);
                            break;
                        case "B":
                            xPos = buffer_read(global.BUFFER, buffer_f32);
                            break;
                        case "C":
                            yPos = buffer_read(global.BUFFER, buffer_f32);
                            break;
                        case "D":
                            zPos = buffer_read(global.BUFFER, buffer_f32);
                            break;
                        case "E":
                            xRot = buffer_read(global.BUFFER, buffer_u32);
                            break;
                        case "F":
                            yRot = buffer_read(global.BUFFER, buffer_u32);
                            break;
                        case "G":
                            zRot = buffer_read(global.BUFFER, buffer_u32);
                            break;
                        case "H":
                            xScale = buffer_read(global.BUFFER, buffer_f32);
                            break;
                        case "I":
                            yScale = buffer_read(global.BUFFER, buffer_f32);
                            break;
                        case "J":
                            zScale = buffer_read(global.BUFFER, buffer_f32);
                            break;
                        case "K":
                            partColorHead = buffer_read(global.BUFFER, buffer_u32);
                            break;
                        case "L":
                            partColorTorso = buffer_read(global.BUFFER, buffer_u32);
                            break;
                        case "M":
                            partColorLArm = buffer_read(global.BUFFER, buffer_u32);
                            break;
                        case "N":
                            partColorRArm = buffer_read(global.BUFFER, buffer_u32);
                            break;
                        case "O":
                            partColorLLeg = buffer_read(global.BUFFER, buffer_u32);
                            break;
                        case "P":
                            partColorRLeg = buffer_read(global.BUFFER, buffer_u32);
                            break;
                        case "Q":
                            partStickerFace = buffer_read(global.BUFFER, buffer_u32);
                            FaceDownload = fetch_asset(partStickerFace, "face", "png", false)
                            /*
                            if string_length(figure.partStickerFace) == 8 {
                                figure.FaceDownload = download("http://www.brick-hill.com/API/client/asset_texture?id="+figure.partStickerFace+"&type=face",global.APPDATA+"face"+string(figure.id)+".png");
                            } else {
                                figure.FaceDownload = -1;
                                figure.Face = -1;
                            }
                            */
                            break;
                        /*
                        case "R":
                            figure.partStickerTShirt = buffer_read_string(global.BUFFER);
                            if string_length(figure.partStickerTShirt) == 8 {
                                figure.TShirtDownload = download("http://www.brick-hill.com/API/client/asset_texture?id="+figure.partStickerTShirt+"&type=tshirt",global.APPDATA+"tshirt"+string(figure.id)+".png");
                            } else {
                                figure.TShirtDownload = -1;
                                figure.TShirt = -1;
                            }
                            break;
                        case "S":
                            figure.partStickerShirt = buffer_read_string(global.BUFFER);
                            if string_length(figure.partStickerShirt) == 8 {
                                figure.ShirtDownload = download("http://www.brick-hill.com/API/client/asset_texture?id="+figure.partStickerShirt+"&type=shirt",global.APPDATA+"shirt"+string(figure.id)+".png");
                            } else {
                                figure.ShirtDownload = -1;
                                figure.Shirt = -1;
                            }
                            break;
                        case "T":
                            figure.partStickerPants = buffer_read_string(global.BUFFER);
                            if string_length(figure.partStickerPants) == 8 {
                                figure.PantsDownload = download("http://www.brick-hill.com/API/client/asset_texture?id="+figure.partStickerPants+"&type=pants",global.APPDATA+"pants"+string(figure.id)+".png");
                            } else {
                                figure.PantsDownload = -1;
                                figure.Pants = -1;
                            }
                            break;
                        */
                        case "U":
                            partModelHat1 = buffer_read(global.BUFFER, buffer_u32);
                            Hat1ModDownload = fetch_asset(partModelHat1, "hat_mod1", "obj", false)
                            Hat1TexDownload = fetch_asset(partModelHat1, "hat_tex1", "png", false)
                            break;
                        case "V":
                            partModelHat2 = buffer_read(global.BUFFER, buffer_u32);
                            Hat2ModDownload = fetch_asset(partModelHat2, "hat_mod2", "obj", false)
                            Hat2TexDownload = fetch_asset(partModelHat2, "hat_tex2", "png", false)
                            break;
                        case "W":
                            partModelHat3 = buffer_read(global.BUFFER, buffer_u32);
                            Hat3ModDownload = fetch_asset(partModelHat3, "hat_mod3", "obj", false)
                            Hat3TexDownload = fetch_asset(partModelHat3, "hat_tex3", "png", false)
                            break;
                        case "X":
                            Speech = buffer_read(global.BUFFER, buffer_string);
                            break;
                    }
                }
            
            }
            break;
        case 13:
            var action,pID,diameter,PxPos,PyPos,PzPos,dir,vel,color;
            action = buffer_read(global.BUFFER, buffer_u8);
            pID = buffer_read(global.BUFFER, buffer_u32);
            if(action == 1) {
                diameter = buffer_read(global.BUFFER, buffer_u32);
                color = buffer_read(global.BUFFER, buffer_u32);
                PxPos = buffer_read(global.BUFFER, buffer_f32);
                PyPos = buffer_read(global.BUFFER, buffer_f32);
                PzPos = buffer_read(global.BUFFER, buffer_f32);
                dir = buffer_read(global.BUFFER, buffer_u32);
                zdir = buffer_read(global.BUFFER, buffer_u32);
                vel = buffer_read(global.BUFFER, buffer_u32);
                new_projectile(pID,diameter,color,PxPos,PyPos,PzPos,dir,zdir,vel);
            } else {
                with obj_projectile {
                    if(projectileID == pID) {
                        GmnDestroyBody(global.set,Object);
                        instance_destroy();
                    }
                }
            }
            break;
        case 14: // CLEAR MAP PACKET
            with obj_brick {
                GmnDestroyBody(global.set, body);
                instance_destroy()
            }
            break
        case 15: // DELETE BOT
            figure_id = buffer_read(global.BUFFER, buffer_u32);

            with obj_dummy {
                if(figureID == figure_id) {
                    instance_destroy()
                }
            }
        case 16: // DELETE BRICKS
            var brickAmount;
            brickAmount = buffer_read(global.BUFFER, buffer_u32)
            
            var i;
            for (i = 0; i < brickAmount; i+=1) {
                var brickId;
                brickId = buffer_read(global.BUFFER, buffer_u32);
                with obj_brick {
                    if brickID == brickId {
                        instance_destroy()
                    }
                }
            }
            
    }
}

var state;

state = socket_get_state(SOCKET);

if state = 2 and !Connected {
    Connected = true;
} else if state = 4 {
    socket_destroy(SOCKET)
    show_message("This server has shutdown.");
    game_end();
    exit;
} else if state = 5 {
    if Connected {
        show_message("Connection lost with the server.");
    } else {
        show_message("Cannot find server.");
    }
    socket_destroy(SOCKET);
    game_end();
    exit;
}

if textbox_focus == -1 && !PAUSED {
    player_movement();
}

player_physics();
camera_update();

if keyboard_check_pressed(vk_escape) {
    PAUSED = !PAUSED;
}

if keyboard_check_pressed(ord("T")) {
    if textbox_focus == -1 {
        textbox_focus = chatBox;
        keyboard_string = "";
    }
    /*var chat;
    chat = get_string("","");
    if(string_char_at(chat,1) != "/") {
        send_command("chat", chat);
    } else {
        var command, args;
        chat = string_replace(chat,"/","");
        
        command = string_split(chat," ",0);
        args = string_replace(chat,command+" ","");
        
        send_command(command, args);
    }*/
} else if keyboard_check_pressed(vk_enter) {
    if textbox_focus == chatBox {
        var chat;
        chat = chatBox.text;
        chatBox.text = "";
        textbox_focus = -1;
        if string_replace_all(chat," ","") != "" {
            if(string_char_at(chat,1) != "/") {
                send_command("chat", chat);
            } else {
                var command, args;
                chat = string_replace(chat,"/","");
                
                command = string_split(chat," ",0);
                args = string_replace(chat,command+" ","");
                
                send_command(command, args);
            }
        }
    }
} else if keyboard_check_pressed(vk_escape) {
    textbox_focus = -1;
}

// Send player input
var currentKey, mouseClick;
currentKey = getKeyString()
mouseClick = mouse_check_button_pressed(mb_left);

if textbox_focus != chatBox && (mouseClick || currentKey != "none") {
    buffer_clear(global.BUFFER);
    buffer_write_uint8(global.BUFFER, buffer_u8, 6);
    buffer_write_uint8(global.BUFFER,buffer_u8, mouseClick)
    buffer_write_string(global.BUFFER, buffer_string, currentKey)
    socket_write_message(SOCKET, global.BUFFER);
}

// Send player coords
if xPrev != xPos || yPrev != yPos || zPrev != zPos || zRPRev != zRot {
    buffer_delete(global.BUFFER);
    buffer_write(global.BUFFER, buffer_f32, xPos);
    buffer_write(global.BUFFER, buffer_f32, yPos);
    buffer_write(global.BUFFER, buffer_f32, zPos);
    buffer_write(global.BUFFER, buffer_u32, floor(zRot));
    socket_write_message(SOCKET, global.BUFFER)
}

/* */
/*  */
