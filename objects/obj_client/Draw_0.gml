draw_set_font(fnt_bold);

draw_world();

d3d_set_projection_ortho(0,0,room_width,room_height,0);
texture_set_interpolation(false);
d3d_set_lighting(false);
d3d_set_perspective(false);
d3d_set_hidden(false);

if(debug != "")
    draw_chat(room_width-10-string_width(string_hash_to_newline(debug)),room_height-10-string_height(string_hash_to_newline(debug)),string_replace_all(debug,"\\c","\\\\c"));
debug = "";

draw_set_alpha(1)

draw_health();

var ch,ch_n,pl,p,player,fg,fig,figure;
draw_set_color(c_black);
pl = 0;
player[pl] = id;
with obj_figure {
    pl += 1;
    player[pl] = self.id;
}
fg = 0;
with obj_dummy {
    figure[fg] = self.id;
    fg += 1;
}

with obj_brick {
    draw_brick_click(id)
}

convertPrepare(obj_client.CamXPos,obj_client.CamYPos,obj_client.CamZPos,obj_client.CamXTo,obj_client.CamYTo,obj_client.CamZTo,0,0,1,obj_client.FOV,room_width/room_height);

for(fg = 0; fg < instance_number(obj_dummy); fg += 1) {
    if figure[fg].Speech != "" {
        draw_speech(figure[fg])
    }
}

for(p = 0; p <= pl; p += 1) {
    if player[p].Speech != "" {
        draw_speech(player[p])
    }
}


/*

        if power(fig.xPos-xPos,2)+power(fig.yPos-yPos,2)+power(fig.zPos-zPos,2) <= 10000 {
            if GmnWorldRayCastDist(global.set,CamXPos,CamYPos,CamZPos,fig.xPos,fig.yPos,fig.zPos+6*fig.zScale) == -1 {
                convert_3d(fig.xPos,fig.yPos,fig.zPos+6*fig.zScale);
                var xText,yText;
                xText = round(x_2d-string_width(fig.Speech)/2);
                yText = round(y_2d-string_height(fig.Speech)-10);
                draw_set_color(0);
                draw_set_alpha(0.4);
                draw_roundrect(xText-10,yText-10,xText+string_width(fig.Speech)+10,yText+string_height(fig.Speech)+10,0);
                draw_sprite_ext(spr_speech,0,xText+string_width(fig.Speech)/2,yText+string_height(fig.Speech)+11,1,1,0,1,0.4);
                
                draw_set_color(c_white);
                draw_set_alpha(1);
                draw_chat(xText,yText,fig.Speech);
            }
        }
    }
}*/

if instance_number(obj_team) <= 0 {
    for(p = 0; p <= pl; p += 1) {
        if(player[p] != id && player[p].alive) {
            if GmnWorldRayCastDist(global.set,CamXPos,CamYPos,CamZPos,player[p].xPos,player[p].yPos,player[p].zPos+6*player[p].zScale) == -1 {
                convert_3d(player[p].xPos,player[p].yPos,player[p].zPos+6*player[p].zScale);
                draw_chat(x_2d-string_width(string_hash_to_newline(player[p].name))/2,y_2d-string_height(string_hash_to_newline(player[p].name))/2,player[p].name);
            }
        }
        if player[p].admin {
                if player[p].admin && player[p].user_id == 2760 { // SECRET GO AWAY !!!!!!!!!
                    draw_sprite(spr_vap,0,room_width-200,10+p*string_height(string_hash_to_newline(" ")));
                    draw_chat(room_width-200+25,10+p*string_height(string_hash_to_newline(" ")), "<color:F5B942>" + string_limit(player[p].name, 160));
                    draw_chat(room_width-string_width(string_hash_to_newline(string(player[p].Score)))-10,10+p*string_height(string_hash_to_newline(" ")),player[p].Score);
                } else {
                    draw_sprite(spr_admin,0,room_width-200,10+p*string_height(string_hash_to_newline(" ")));
                    draw_chat(room_width-200+25,10+p*string_height(string_hash_to_newline(" ")), "<color:0ADEFF>" + string_limit(player[p].name, 160));
                    draw_chat(room_width-string_width(string_hash_to_newline(string(player[p].Score)))-10,10+p*string_height(string_hash_to_newline(" ")),player[p].Score);   
                }
        } else {
            // The player has a membership
            var member;
            member = player[p].membership
            if member > 1 {
                draw_sprite(get_membership(member),0,room_width-200,10+p*string_height(string_hash_to_newline(" ")));
            }
            draw_chat(room_width-200+25,10+p*string_height(string_hash_to_newline(" ")),string_limit(player[p].name,160));
            draw_chat(room_width-string_width(string_hash_to_newline(string(player[p].Score)))-10,10+p*string_height(string_hash_to_newline(" ")),player[p].Score);
        }
    }
} else {
    var nextY,teamCount;
    teamCount = 0;
    nextY = 10;
    with obj_team {
        var team_col,tp,hex;
        hex = dec_to_hex(color);
        team_col = "<color:"+string_repeat("0",6-string_length(hex))+hex+">";
        tp = 0;
        draw_set_font(fnt_big);
        draw_chat(room_width-200,nextY,team_col+name);
        draw_set_font(fnt_bold);
        nextY += string_height(string_hash_to_newline(" "))+4;
        for(p = 0; p <= pl; p += 1) {
            if(player[p].team == teamID) {
                if(player[p] != other.id) {
                    with other.id {
                        if GmnWorldRayCastDist(global.set,CamXPos,CamYPos,CamZPos,player[p].xPos,player[p].yPos,player[p].zPos+6*player[p].zScale) == -1 {
                            convert_3d(player[p].xPos,player[p].yPos,player[p].zPos+6*player[p].zScale);
                            draw_chat(x_2d-string_width(string_hash_to_newline(player[p].name))/2,y_2d-string_height(string_hash_to_newline(player[p].name))/2,team_col+player[p].name);
                        }
                    }
                }
                if player[p].admin {
                    if player[p].admin && player[p].user_id == 2760 {
                        draw_sprite(spr_vap,0,room_width-200,nextY);
                        draw_chat(room_width-200+25,nextY,"<color:F5B942>" + string_limit(player[p].name,160));
                        draw_chat(room_width-string_width(string_hash_to_newline(string(player[p].Score)))-10,nextY,player[p].Score);
                    } else {
                        draw_sprite(spr_admin,0,room_width-200,nextY);
                        draw_chat(room_width-200+25,nextY,"<color:0ADEFF>" + string_limit(player[p].name,160));
                        draw_chat(room_width-string_width(string_hash_to_newline(string(player[p].Score)))-10,nextY,player[p].Score);
                    }
                } else {
                    // The player has a membership
                    var member;
                    member = player[p].membership
                    if member > 1 {
                        draw_sprite(get_membership(member),0,room_width-200,nextY)
                    }
                    draw_chat(room_width-200+25,nextY,string_limit(player[p].name,160));
                    draw_chat(room_width-string_width(string_hash_to_newline(string(player[p].Score)))-10,nextY,player[p].Score);
                }
                //draw_chat(room_width-200,nextY,team_col+string_limit(player[p].name,160));
                //draw_chat(room_width-string_width(string(player[p].Score))-10,nextY,team_col+string(player[p].Score));
                nextY += string_height(string_hash_to_newline(" "));
            }
            tp += 1;
        }
        teamCount += 1;
    }
}

ch_n = 0;
for(ch = 0; ch <= 10; ch += 1) {
    if(ds_list_size(CHAT)-10+ch >= 0 && ds_list_size(CHAT)-10+ch < ds_list_size(CHAT)) {
        draw_chat(10,10+ch_n*string_height(string_hash_to_newline(" ")),ds_list_find_value(CHAT,ds_list_size(CHAT)-10+ch));
        ch_n += 1;
    }
}
if textbox_focus == chatBox {
    draw_set_alpha(0.4);
    draw_set_color(0);
    draw_roundrect(6,16+10*string_height(string_hash_to_newline(" ")),224,20+11*string_height(string_hash_to_newline(" ")),0);
}

draw_set_alpha(1);
draw_set_color(c_white);

textbox_draw(chatBox,10,18+10*string_height(string_hash_to_newline(" ")),220,18+11*string_height(string_hash_to_newline(" "))+8);

if (!Connected) {
    draw_set_alpha(0.5);
    draw_set_color(c_white);
    draw_rectangle(room_width/2-80,room_height/2-30,room_width/2+80,room_height/2+30,0);
    
    draw_set_alpha(1);
    draw_set_color(c_white);
    draw_set_halign(fa_center);
    draw_set_valign(fa_middle);
    
    draw_text(room_width/2,room_height/2,string_hash_to_newline("Waiting for server"));

    draw_set_halign(fa_left);
    draw_set_valign(fa_top);
    exit;
}

if (!authenticated) {
    draw_set_alpha(0.5);
    draw_set_color(c_white);
    draw_rectangle(room_width/2-80,room_height/2-30,room_width/2+80,room_height/2+30,0);
    draw_set_alpha(1);
    draw_set_color(c_white);
    draw_set_halign(fa_center);
    draw_set_valign(fa_middle);
    draw_text(room_width/2,room_height/2,string_hash_to_newline("Waiting for authentication"));
    draw_set_halign(fa_left);
    draw_set_valign(fa_top);
    exit;
}

if (!bricksDownloaded && brickCount > 0) {    
    draw_set_alpha(0.5);
    draw_set_color(c_white);
    draw_rectangle(room_width/2-80,room_height/2-30,room_width/2+80,room_height/2+30,0);
    draw_set_alpha(1);
    draw_set_color(c_white);
    draw_set_halign(fa_center);
    draw_set_valign(fa_middle);
    draw_text(room_width/2,room_height/2,string_hash_to_newline("Bricks: "+string(instance_number(obj_brick))+"/"+string(brickCount)));
    exit;
}

var slotNum;
slotNum = 0;
with obj_slot {
    draw_set_alpha(0.4);
    draw_set_color(c_black);
    draw_rectangle(slotNum*100,room_height-100,(slotNum+1)*100,room_height,0);
    draw_set_color(c_white);
    draw_set_alpha(1);
    draw_text(slotNum*100+string_width(string_hash_to_newline(name))/2,room_height-50-string_height(string_hash_to_newline(name))/2,string_hash_to_newline(name));
    if obj_client.Arm == slotID {
        //draw border
        draw_set_color(c_aqua);
        draw_rectangle(slotNum*100,room_height-100,(slotNum+1)*100,room_height,1);
        draw_rectangle(slotNum*100+1,room_height-99,(slotNum+1)*100-1,room_height-1,1);
    }
    slotNum += 1;
}

draw_set_alpha(1);
with obj_topPrint {
    if current_time < stop {
        draw_chat((room_width-string_width(string_hash_to_newline(text)))/2,10,text);
    }
}
with obj_centerPrint {
    if current_time < stop {
        draw_chat((room_width-string_width(string_hash_to_newline(text)))/2,(room_height-string_height(string_hash_to_newline(text)))/2,text);
    }
}
with obj_bottomPrint {
    if current_time < stop {
        draw_chat((room_width-string_width(string_hash_to_newline(text)))/2,room_height-string_height(string_hash_to_newline(text))-10,text);
    }
}

if (PAUSED) {
    draw_pause();
}

if keyboard_check(vk_f1) {
    draw_set_color(c_white);
    draw_chat(10,room_height-10-string_height(string_hash_to_newline(" ")),"FPS: "+string(fps));
}


/* */
/*  */
