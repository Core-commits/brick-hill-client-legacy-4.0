global.APPDATA = environment_get_variable("APPDATA")+"\\Brick Hill\\";
global.ASSET_DIR = global.APPDATA + "assets\\"

if !directory_exists(global.APPDATA) {directory_create(global.APPDATA);}
if !directory_exists(global.ASSET_DIR) {directory_create(global.ASSET_DIR);}

SOCKET = false

if (!get_params()) {
    game_end()
    exit;
}



debug = "";

d3d_start();

settings_create();

camera_create();

world_create();

window_ini();

server_connect();

//get_avatar();

chat_ini();

chatBox = textbox_create();
chatBox.text = "Press 't' to talk";
chatBox.single_line = true;
chatBox.max_chars = 84;
chatBox.select_on_focus = true;

Arch = d3d_model_create();
d3d_model_load(Arch, temp_directory+"\\arch.d3d");

Brick = d3d_model_create();
d3d_model_load(Brick, temp_directory+"\\brick.d3d");

Corner = d3d_model_create();
d3d_model_load(Corner, temp_directory+"\\corner.d3d");

Corner_Inverted = d3d_model_create();
d3d_model_load(Corner_Inverted, temp_directory+"\\corner_inv.d3d");

Dome = d3d_model_create();
d3d_model_load(Dome, temp_directory+"\\dome.d3d");

Fence_Bottom = d3d_model_create();
d3d_model_load(Fence_Bottom, temp_directory+"\\fence_bottom.d3d");
Fence_Middle = d3d_model_create();
d3d_model_load(Fence_Middle, temp_directory+"\\fence_middle.d3d");
Fence_Top = d3d_model_create();
d3d_model_load(Fence_Top, temp_directory+"\\fence_top.d3d");

Flag = d3d_model_create();
d3d_model_load(Flag, temp_directory+"\\flag.d3d");

Flagpole_Base = d3d_model_create();
d3d_model_load(Flagpole_Base, temp_directory+"\\flagpole_base.d3d");
Flagpole_Middle = d3d_model_create();
d3d_model_load(Flagpole_Middle, temp_directory+"\\flagpole_middle.d3d");
Flagpole_Top = d3d_model_create();
d3d_model_load(Flagpole_Top, temp_directory+"\\flagpole_top.d3d");

Round_Brick = d3d_model_create();
d3d_model_load(Round_Brick, temp_directory+"\\round_brick.d3d");

Round_Large1x1 = d3d_model_create();
d3d_model_load(Round_Large1x1, temp_directory+"\\round_large1x1.d3d");

Round_Small1x1 = d3d_model_create();
d3d_model_load(Round_Small1x1, temp_directory+"\\round_small1x1.d3d");

Slope = d3d_model_create();
d3d_model_load(Slope, temp_directory+"\\slope.d3d");

Vent = d3d_model_create();
d3d_model_load(Vent, temp_directory+"\\vent.d3d");

name = "Player"

user_id = -1

admin = false;

PAUSED = false;

// it's the server telling the client they're authenticated you greasy skidd
authenticated = false

membership = 0

Face = -1;
Shirt = -1;
TShirt = -1;
Pants = -1;

Hat1_Tex = -1;
Hat2_Tex = -1;
Hat3_Tex = -1;

Hat1 = -1;
Hat2 = -1;
Hat3 = -1;

cachedItems = ds_map_create();

// Downloads
FaceDownload = -1;
TShirtDownload = -1;
ShirtDownload = -1;
PantsDownload = -1;
Hat1TexDownload = -1;
Hat1ModDownload = -1;
Hat2TexDownload = -1;
Hat2ModDownload = -1;
Hat3TexDownload = -1;
Hat3ModDownload = -1;

xPrev = 0;
yPrev = 0;
zPrev = 0;
lookingAt = -1;
lookingAtXPos = 0;
lookingAtYPos = 0;
lookingAtZPos = 0;

maxHealth = 100
maxSpeed = 4
animation = 0;
walking = false;
frame = 0;
Dist = 0;
Speech = ""

Ambient = 0;
SkyColor = $e6b271;
BasePlateColor = $248233;
BasePlateSize = 0;
BasePlateAlpha = 1;
SunIntensity = 200;

stud = background_get_texture(bkg_stud);

modelHead = d3d_model_create();
d3d_model_load(modelHead, temp_directory+"\\Head.d3d");
modelTorso = d3d_model_create();
d3d_model_load(modelTorso, temp_directory+"\\Torso.d3d");
modelTShirt = d3d_model_create();
d3d_model_load(modelTShirt, temp_directory+"\\TShirt.d3d");
modelLArm = d3d_model_create();
d3d_model_load(modelLArm, temp_directory+"\\LeftArm.d3d");
modelRArm = d3d_model_create();
d3d_model_load(modelRArm, temp_directory+"\\RightArm.d3d");
modelLLeg = d3d_model_create();
d3d_model_load(modelLLeg, temp_directory+"\\LeftLeg.d3d");
modelRLeg = d3d_model_create();
d3d_model_load(modelRLeg, temp_directory+"\\RightLeg.d3d");

