{
    "id": "d38292bc-1703-4f10-b279-ad3aafbee46c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_client",
    "eventList": [
        {
            "id": "892cc892-ea1a-44f5-a9fe-9ab412be50c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d38292bc-1703-4f10-b279-ad3aafbee46c"
        },
        {
            "id": "1bb5a087-546c-46df-b260-bec47706fd2a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "d38292bc-1703-4f10-b279-ad3aafbee46c"
        },
        {
            "id": "b0a87860-5b1f-4b54-b582-e67d6238aae2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d38292bc-1703-4f10-b279-ad3aafbee46c"
        },
        {
            "id": "0b62d389-88da-4fe1-a6f4-32d4e3b04f20",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 30,
            "eventtype": 7,
            "m_owner": "d38292bc-1703-4f10-b279-ad3aafbee46c"
        },
        {
            "id": "7ef29a1d-7472-43e3-9c6f-989ed7054c61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "d38292bc-1703-4f10-b279-ad3aafbee46c"
        },
        {
            "id": "b98ae19d-99ba-488a-9a4b-1c283cfac458",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "d38292bc-1703-4f10-b279-ad3aafbee46c"
        },
        {
            "id": "76b3e126-30f2-45c0-bccd-d66545f5d9d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "d38292bc-1703-4f10-b279-ad3aafbee46c"
        },
        {
            "id": "9c972d60-9f61-4179-af86-cf821da5f265",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "d38292bc-1703-4f10-b279-ad3aafbee46c"
        },
        {
            "id": "22394dca-aeb1-4d33-bb2a-0a454a0e6508",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d38292bc-1703-4f10-b279-ad3aafbee46c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}