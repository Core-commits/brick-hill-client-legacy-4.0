xPos=GmnBodyGetPosition(Object,0);
yPos=GmnBodyGetPosition(Object,1);
zPos=GmnBodyGetPosition(Object,2);

xRot=radtodeg(GmnBodyGetRotation(Object,0));
yRot=radtodeg(GmnBodyGetRotation(Object,1));
zRot=radtodeg(GmnBodyGetRotation(Object,2));

var SCALE,ORIGIN;
SCALE[0] = Diameter/2;
SCALE[1] = Diameter/2;
SCALE[2] = Diameter/2;
ORIGIN[0] = xPos+SCALE[0];
ORIGIN[1] = yPos+SCALE[1];
ORIGIN[2] = zPos+SCALE[2];

with obj_figure {
    var size,center;
    size[0] = xScale;
    size[1] = yScale;
    size[2] = 5*zScale/2;
    center[0] = xPos;
    center[1] = yPos;
    center[2] = zPos+size[2];
    
    var i,dist,close,COL;
    COL = true;
    for(i=0;i<3;i+=1) {
        dist = abs(ORIGIN[i]-center[i]);
        close = size[i]+SCALE[i];
        if(dist >= close+0.4) {
            COL = false;
        }
    }
    if(COL) {
        buffer_clear(global.BUFFER);
        buffer_write(global.BUFFER, buffer_u8, 4);
        buffer_write(global.BUFFER, buffer_u32, other.projectileID);
        buffer_write(global.BUFFER, buffer_u32, net_id);
        socket_write_message(obj_client.SOCKET,  global.BUFFER);
    }
}

