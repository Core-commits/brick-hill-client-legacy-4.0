if result {
    return instance_destroy()
}

var state;

httprequest_update(httprequest);

state = httprequest_get_state(httprequest);

if state == 5 {
    httprequest_destroy(httprequest)
    return instance_destroy()
}

if state == 4 {
    if (!redirected) {
        redirected = true
        location = httprequest_find_response_header(httprequest, "location")
        location = string_delete(location, 5, 1) // bangla way of deleting https
        httprequest_reset(httprequest)
        httprequest_connect(httprequest, location, false);
    } else {
        var b;
        b = buffer_create();
        
        httprequest_get_message_body_buffer(httprequest, b);
        
        httprequest_destroy(httprequest)
        
        buffer_write_to_file(b, file);
        
        buffer_destroy(b);
        
        result = true
    }
    redirected = true
}

