
var i, id_string;
id_string = argument0;

for(i = 1; i <= string_length(id_string); i += 1) {
    switch string_char_at(id_string,i) {
        case "A":
            xPos = buffer_read(global.BUFFER, buffer_f32);
            break;
        case "B":
            yPos = buffer_read(global.BUFFER, buffer_f32);
            break;
        case "C":
            zPos = buffer_read(global.BUFFER, buffer_f32);
            break;
        case "D":
            xRot = buffer_read(global.BUFFER, buffer_u32);
            break;
        case "E":
            yRot = buffer_read(global.BUFFER, buffer_u32);
            break;
        case "F":
            zRot = buffer_read(global.BUFFER, buffer_u32);
            break;
        case "G":
            xScale = buffer_read(global.BUFFER, buffer_f32);
            break;
        case "H":
            yScale = buffer_read(global.BUFFER, buffer_f32);
            break;
        case "I":
            zScale = buffer_read(global.BUFFER, buffer_f32);
            break;
            /*
        case "J":
            Arm = buffer_read_int8(global.BUFFER);
            break;
            */
        case "K":
            partColorHead = buffer_read(global.BUFFER, buffer_u32);
            break;
        case "L":
            partColorTorso = buffer_read(global.BUFFER, buffer_u32);
            break;
        case "M":
            partColorLArm = buffer_read(global.BUFFER, buffer_u32);
            break;
        case "N":
            partColorRArm = buffer_read(global.BUFFER, buffer_u32);
            break;
        case "O":
            partColorLLeg = buffer_read(global.BUFFER, buffer_u32);
            break;
        case "P":
            partColorRLeg = buffer_read(global.BUFFER, buffer_u32);
            break;
        case "Q":
            partStickerFace = buffer_read(global.BUFFER, buffer_u32);
            FaceDownload = fetch_asset(partStickerFace, "face", "png", false)
            break;
        case "U":
            partModelHat1 = buffer_read(global.BUFFER, buffer_u32);
            Hat1TexDownload = fetch_asset(partModelHat1, "hat_tex1", "png", false)
            Hat1ModDownload = fetch_asset(partModelHat1, "hat_mod1", "obj", false)
            break;
        case "V":
            partModelHat2 = buffer_read(global.BUFFER, buffer_u32);
            Hat2TexDownload = fetch_asset(partModelHat2, "hat_tex2", "png", false)
            Hat2ModDownload = fetch_asset(partModelHat2, "hat_mod2", "obj", false)
            break;
        case "W":
            partModelHat3 = buffer_read(global.BUFFER, buffer_u32);
            Hat3TexDownload = fetch_asset(partModelHat3, "hat_tex3", "png", false)
            Hat3ModDownload = fetch_asset(partModelHat3, "hat_mod3", "obj", false)
            break;
        case "X":
            Score = buffer_read(global.BUFFER, buffer_u32);
            break;
        case "Y":
            team = buffer_read(global.BUFFER, buffer_u32);
            break;
        
        // LOCAL CHANGES
        case "1":
            maxSpeed = buffer_read(global.BUFFER, buffer_u32);
            break;
        case "2":
            maxJumpHeight = buffer_read(global.BUFFER, buffer_u32);
            break;
        case "3":
            FOV = buffer_read(global.BUFFER, buffer_u32);
            break;
        case "4":
            CamDist = buffer_read(global.BUFFER, buffer_u32);
            break;
        case "5":
            CamXPos = buffer_read(global.BUFFER, buffer_u32);
            break;
        case "6":
            CamYPos = buffer_read(global.BUFFER, buffer_u32);
            break;
        case "7":
            CamZPos = buffer_read(global.BUFFER, buffer_u32);
            break;
        case "8":
            CamXRot = buffer_read(global.BUFFER, buffer_u32);
            break;
        case "9":
            CamYRot = buffer_read(global.BUFFER, buffer_u32);
            break;
        case "a":
            CamZRot = buffer_read(global.BUFFER, buffer_u32);
            break;
        case "b":
            CamType = buffer_read(global.BUFFER, buffer_string);
            break;
        case "c":
            CamObj = buffer_read(global.BUFFER, buffer_u32);
            break;
        case "e":
            var healthVar;
            healthVar = buffer_read(global.BUFFER, buffer_f32);
            if (healthVar > maxHealth) {
                maxHealth = healthVar
            }
            Health = healthVar
            break;
        case "f":
            Speech = buffer_read(global.BUFFER, buffer_string);
            break
        case "g": // Equip tool
            var slotId, model;
            slotId = buffer_read(global.BUFFER, buffer_u32);
            model = buffer_read(global.BUFFER, buffer_u32);
            Item_ModDownload = fetch_asset(model, "item_mod", "obj", false)
            Item_TexDownload = fetch_asset(model, "item_tex", "png", false)
            Arm = slotId
            break
        case "h": // De-equip tool
            Arm = -1;
            break
    }
}
