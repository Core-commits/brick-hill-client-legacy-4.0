// Initialise the global array that allows the lookup of the depth of a given object
// GM2.0 does not have a depth on objects so on import from 1.x a global array is created
// NOTE: MacroExpansion is used to insert the array initialisation at import time
gml_pragma( "global", "__global_object_depths()");

// insert the generated arrays here
global.__objectDepths[0] = 0; // obj_client
global.__objectDepths[1] = 0; // obj_figure
global.__objectDepths[2] = 0; // obj_brick
global.__objectDepths[3] = 0; // obj_topPrint
global.__objectDepths[4] = 0; // obj_centerPrint
global.__objectDepths[5] = 0; // obj_bottomPrint
global.__objectDepths[6] = 0; // obj_team
global.__objectDepths[7] = 0; // obj_slot
global.__objectDepths[8] = 0; // obj_textbox
global.__objectDepths[9] = 0; // obj_download
global.__objectDepths[10] = 0; // obj_dummy
global.__objectDepths[11] = 0; // obj_projectile
global.__objectDepths[12] = 0; // obj_asset_download


global.__objectNames[0] = "obj_client";
global.__objectNames[1] = "obj_figure";
global.__objectNames[2] = "obj_brick";
global.__objectNames[3] = "obj_topPrint";
global.__objectNames[4] = "obj_centerPrint";
global.__objectNames[5] = "obj_bottomPrint";
global.__objectNames[6] = "obj_team";
global.__objectNames[7] = "obj_slot";
global.__objectNames[8] = "obj_textbox";
global.__objectNames[9] = "obj_download";
global.__objectNames[10] = "obj_dummy";
global.__objectNames[11] = "obj_projectile";
global.__objectNames[12] = "obj_asset_download";


// create another array that has the correct entries
var len = array_length_1d(global.__objectDepths);
global.__objectID2Depth = [];
for( var i=0; i<len; ++i ) {
	var objID = asset_get_index( global.__objectNames[i] );
	if (objID >= 0) {
		global.__objectID2Depth[ objID ] = global.__objectDepths[i];
	} // end if
} // end for